package tryout.middle.api;

import tryout.middle.domain.CreditBankAccount;
import tryout.middle.domain.DebitBankAccount;
import tryout.middle.domain.Visitor;

import java.math.BigDecimal;

public class Capitalise implements Visitor {
    @Override
    public void visit(DebitBankAccount debitBankAccount) {
        debitBankAccount.debit(BigDecimal.valueOf(10d));
    }

    @Override
    public void visit(CreditBankAccount creditBankAccount) {
        creditBankAccount.credit(BigDecimal.valueOf(10d));
    }
}
