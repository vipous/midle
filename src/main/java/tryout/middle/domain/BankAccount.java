package tryout.middle.domain;

import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Getter
public abstract class BankAccount implements Visitable {
    private String num;
    private BigDecimal amount;
    private LocalDate open;
    private LocalDate close;
    private LocalDate end;

    public abstract void debit(BigDecimal sum);

    public abstract void credit(BigDecimal sum);

    protected final void operate(BigDecimal sum){
        amount = amount.add(sum);
    }

    public abstract boolean isValidAmmount(BigDecimal sum);

    public static boolean isValidSum(BigDecimal sum){
        return sum.signum()==-1;
    }
}
