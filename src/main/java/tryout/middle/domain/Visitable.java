package tryout.middle.domain;

public interface Visitable{
    void accept(Visitor visitor);
}
