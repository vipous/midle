package tryout.middle.domain;

public interface Visitor{
    void visit(DebitBankAccount debitBankAccount);
    void visit(CreditBankAccount creditBankAccount);
}
